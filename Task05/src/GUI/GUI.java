package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Student.Student_Marks_Filter;
import Student.Student_Marks_Foreach;
import Student.Student_Marks_Lowest;
import Student.Student_Marks_Parellel_Streaming;
import Student.Student_Marks_Sorting;

public class GUI extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	static JButton button;
	static JLabel label;
	static JFrame frame = new JFrame("TASK 05");
	static JPanel panel;
	
	public static void main (String[]args) {
		GUI gui = new GUI();
		
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		panel.add(Box.createVerticalStrut(1));
		button = new JButton("Exam Marks Filter");
		panel.add(button);
		button.addActionListener(gui); 
		panel.add(Box.createVerticalStrut(10));
		
		panel.add(Box.createVerticalStrut(1));
		button = new JButton("Exam Marks Foreach");
		panel.add(button);
		button.addActionListener(gui); 
		panel.add(Box.createVerticalStrut(10));

		panel.add(Box.createVerticalStrut(1));
		button = new JButton("Exam Marks Lowest");
		panel.add(button);
		button.addActionListener(gui); 
		panel.add(Box.createVerticalStrut(10));

		panel.add(Box.createVerticalStrut(1));
		button = new JButton("Exam Marks Parellel Streaming");
		panel.add(button);
		button.addActionListener(gui); 
		panel.add(Box.createVerticalStrut(10));

		panel.add(Box.createVerticalStrut(1));
		button = new JButton("Exam Marks Sorting");
		panel.add(button);
		button.addActionListener(gui); 
		panel.add(Box.createVerticalStrut(10));
		
		frame.setSize(450,240);
		frame.add(panel);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String s = e.getActionCommand(); 
		if (s.equals("Exam Marks Foreach")) { 
			Student_Marks_Filter.readAndWrite();
		} else if (s.equals("Exam Marks Filter")) {
			Student_Marks_Foreach.readAndWrite();
		} else if (s.equals("Exam Marks Parellel Streaming")) { 
			Student_Marks_Lowest.readAndWrite();
		} else if (s.equals("Exam Marks Lowest")) { 
			Student_Marks_Parellel_Streaming.readAndWrite();
		} else if (s.equals("Exam Marks Sorting")) { 
			Student_Marks_Sorting.readAndWrite();
		} 
	}
	
}

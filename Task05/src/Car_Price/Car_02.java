package Car_Price;

import java.util.Arrays;
import java.util.List;

public class Car_02 {

	  public static void main(String[] args) {
		    List<Car> table = Arrays.asList(
		        new Car(1,13328,"LEXUS","RX 450",2010,"Jeep","Hybrid","186005 km","Silver"),
		        
		        new Car(2,8467,"HONDA","FIT",2006,"Hatchback","Petrol","200000 km","Black"),
		        
		        new Car(3,16621,"CHEVROLET","Equinox",2011,"Jeep","Petrol","192000 km","Black"),
		        
		        new Car(4,3607,"FORD","Escape",2011,"Jeep","Hybrid","168966 km","White"),
		        
		        new Car(5,11726,"HONDA","FIT",2014,"Hatchback","Petrol","91901 km","Silver"),
		        
		        new Car(6,39493,"HYUNDAI","Santa FE",2016,"Jeep","Diesel","160931 km","White"),
		        
		        new Car(7,1803,"TOYOTA","Prius",2010,"Hatchback","Hybrid","258909 km","White"),
		        
		        new Car(8,549,"HYUNDAI","Sonata",2013,"Sedan","Petrol","216118 km","Grey"),
		        
		        new Car(9,1098,"TOYOTA","Camry",2014,"Sedan","Hybrid","398069 km","Black"),
		        
		        new Car(10,26657,"LEXUS","RX 350",2007,"Jeep","Petrol","128500 km","Silver"),
		        
		        new Car(11,941,"BENZ","E 350",2014,"Sedan","Diesel","184467 km","White"));

		    table.stream().forEach(x -> System.out.println(x));
		    System.out.println();
		    table.parallelStream().forEach(System.out::println);
		  }
		}

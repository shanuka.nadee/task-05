package Car_Price;

public class Car implements Comparable<Car>{
	private int id;
	private int price;
	private String company;
	private String model;
	private int year;
	private String category;
	private String fule_type;
	private String milage;
	private String color;
	
	
	public Car(int id, int price, String company, String model, int year,
			String category, String fule_type, String milage, String color) {
		this.id = id;
		this.price = price;
		this.company = company;
		this.model = model;
		this.year = year;
		this.category = category;
		this.fule_type = fule_type;
		this.milage = milage;
		this.color = color;
		
	}
	
	public String toString() {
		return String.format("%-3d%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s", id, price, company,model,year,category,fule_type,milage,color);
	}
	


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getFule_type() {
		return fule_type;
	}

	public void setFule_type(String fule_type) {
		this.fule_type = fule_type;
	}

	public String getMilage() {
		return milage;
	}

	public void setMilage(String milage) {
		this.milage = milage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public int compareTo(Car o) {
		// TODO Auto-generated method stub
		return 0;
	}

	
}

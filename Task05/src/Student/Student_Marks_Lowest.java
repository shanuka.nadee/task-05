package Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;



public class Student_Marks_Lowest {
	public static void main(String[] args) {
		readAndWrite();
	}

	public static void readAndWrite() {
		List<Student_Marks_Details> table = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("ExamMarksDetails.csv"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        table.add(new Student_Marks_Details(
		        		Integer.parseInt(values[0]),
		        		values[1].toString(),
		        		Integer.parseInt(values[2]),
		        		Integer.parseInt(values[3].toString()),
		        		Integer.parseInt(values[4].toString()),
		        		Integer.parseInt(values[5].toString()),
		        		Integer.parseInt(values[6].toString()),
		        		Double.parseDouble(values[7].toString())));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		

		OptionalInt min = table.stream().mapToInt(Student_Marks_Details::getTOTAl_Marks).min();
	    if (min.isPresent()) {
	      System.out.printf("\nLowest Total Mark is %d\n", min.getAsInt());
	    } else {
	      System.out.println("Failed !");
	    }

	    System.out.print("Marks of Chemistry Studies is ");
	    Integer result = table.stream().map(Student_Marks_Details::getChemistry_Marks).reduce(0, (a, b) -> a + b);
	    System.out.println(result);
	    

	    List<String> s = table.stream()
	    	    .filter(p -> p.getChemistry_Marks() < 50)
	    	    .map(Student_Marks_Details::getStudent_Name)
	    	    .collect(Collectors.toList());
	    	                   
	    	System.out.println("Student who got less than 50 marks for Chemistry :" +
	    	    s.toString());
	    	
		    
    	try {
		      FileWriter writer = new FileWriter("src/Textfile/ExamMarkLowest.txt");
		      writer.write("Lowest Exam Mark " + min.getAsInt() + "\n");
		      writer.write("Total Marks of Chemistry Studies : " + result + "\n");
		      writer.write("Students who got less than 50 marks for BS : " + s + "\n");
		      writer.close();
		      System.out.println("Created Exam Mark Lowest Details Successfully!");
		    } catch (IOException e) {
		      System.out.println("Process Faield. Try Again !");
		      e.printStackTrace();
		    }
	}
	
}

package Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class Student_Marks_Filter {
	
	public static void main(String[] args) {
		readAndWrite();
	}

	public static void readAndWrite() {
		List<Student_Marks_Details> table = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("ExamMarksDetails.csv"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        table.add(new Student_Marks_Details(
		        		Integer.parseInt(values[0]),
		        		values[1].toString(),
		        		Integer.parseInt(values[2]),
		        		Integer.parseInt(values[3].toString()),
		        		Integer.parseInt(values[4].toString()),
		        		Integer.parseInt(values[5].toString()),
		        		Integer.parseInt(values[6].toString()),
		        		Double.parseDouble(values[7].toString())));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("\nStudents Who Got Total Mark above 250");
	    table.stream().filter(student -> student.getTOTAl_Marks()  > 250)
	        .forEach(System.out::println);

	    System.out.println();
	    System.out.println("Student Who Got 50 as a Chemistry Mark");
	    table.stream().filter(student -> student.getChemistry_Marks() > 50)
	        .forEach(System.out::println);
	    
	    try {
		      FileWriter writer = new FileWriter("src/Textfile/ExamMarkFilter.txt");
		      writer.write("Student Who Got Total Mark above 250\n");
		      table.stream().filter(student -> student.getTOTAl_Marks() > 250)
		        .forEach(str -> {
		        	try {
						writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      
		      writer.write("\nStudent Who Got 50 as a Chemistry Mark\n");
		      table.stream().filter(student -> student.getChemistry_Marks() > 50)
		        .forEach(str -> {
		        	try {
						writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      writer.close();
		      System.out.println("Created Exam Mark For Each Successfully !");
		    } catch (IOException e) {
		      System.out.println("Process Faield. Try Again !");
		      e.printStackTrace();
		    }
	}
	
}

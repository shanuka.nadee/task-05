package Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Student_Marks_Parellel_Streaming {
	public static void main(String[] args) {
		readAndWrite();
	}

	public static void readAndWrite() {
		List<Student_Marks_Details> table = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("ExamMarksDetails.csv"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        table.add(new Student_Marks_Details(
		        		Integer.parseInt(values[0]),
		        		values[1].toString(),
		        		Integer.parseInt(values[2]),
		        		Integer.parseInt(values[3].toString()),
		        		Integer.parseInt(values[4].toString()),
		        		Integer.parseInt(values[5].toString()),
		        		Integer.parseInt(values[6].toString()),
		        		Double.parseDouble(values[7].toString())));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		table.stream().forEach(x -> System.out.println(x));
	    System.out.println();
	    table.parallelStream().forEach(System.out::println);
	    

	    try {
		      FileWriter writer = new FileWriter("src/Textfile/ExamMarkParellelStreaming.txt");
		      table.stream()
		        .forEach(x -> {
		        	try {
						writer.write(x.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      
		      writer.write("\nParallel Stream\n");
		      table.parallelStream()
		      .forEach(x -> {
		        	try {
						writer.write(x.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      writer.close();
		      System.out.println("Created Exam Mark Parellel Streaming Successfully.");
		    } catch (IOException e) {
		      System.out.println("Process Faield. Try Again !");
		      e.printStackTrace();
		    }
	}
	
}

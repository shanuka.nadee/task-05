package Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Student_Marks_Sorting {
	public static void main(String[] args) {
		readAndWrite();
	}

	public static void readAndWrite() {
		List<Student_Marks_Details> table = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("ExamMarksDetails.csv"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        table.add(new Student_Marks_Details(
		        		Integer.parseInt(values[0]),
		        		values[1].toString(),
		        		Integer.parseInt(values[2]),
		        		Integer.parseInt(values[3].toString()),
		        		Integer.parseInt(values[4].toString()),
		        		Integer.parseInt(values[5].toString()),
		        		Integer.parseInt(values[6].toString()),
		        		Double.parseDouble(values[7].toString())));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("\nSorted by Position - Descending");
	    table.stream().sorted().forEach(System.out::println);

	    System.out.println();
	    System.out.println("Sorted by Student Name - Ascending");
	    table.stream()
	         .sorted((c1, c2) -> 
	            ((String) c1.getStudent_Name()).compareTo(c2.getStudent_Name()))
	         .forEach(System.out::println);

	    try {
		      FileWriter writer = new FileWriter("ExamMarkSorting.txt");
		      writer.write("Sorted by Position - Descending\n");
		      table.stream().sorted()
		        .forEach(str -> {
		        	try {
						writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      
		      writer.write("\nSorted by Student Name - Ascending\n");
		      table.stream().sorted((c1, c2) -> 
	            ((String) c1.getStudent_Name()).compareTo(c2.getStudent_Name()))
		        .forEach(str -> {
		        	try {
						writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      writer.close();
		      System.out.println("Created Exam Mark For Each Successfully !");
		    } catch (IOException e) {
		      System.out.println("Process Faield. Try Again !");
		      e.printStackTrace();
		    }
	}
	
}

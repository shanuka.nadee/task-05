package Student;


public class Student_Marks_Details implements Comparable<Student_Marks_Details>{
	private int Student_Position;
	private String Student_Name;
	private int Physics_Marks;
	private int Chemistry_Marks;
	private int ICT_Marks;
	private int English_Marks;
	private int TOTAl_Marks;
	private double Average;
	
	
	public Student_Marks_Details(int studentPosition, String studentName, int physicsMarks, int chemistryMarks, int ictMarks,int engMark, int totalMark, double examAvg) {
		this.Student_Position = studentPosition;
		this.Student_Name = studentName;
		this.Physics_Marks = physicsMarks;
		this.Chemistry_Marks = chemistryMarks;
		this.ICT_Marks = ictMarks;
		this.English_Marks=engMark;
		this.TOTAl_Marks = totalMark;
		this.Average = examAvg;
		
	}
	
	public int compareTo(Student_Marks_Details examMarkDetails) {
		return ((Integer)TOTAl_Marks).compareTo(examMarkDetails.TOTAl_Marks);
	}
	
	public String toString() {
		return String.format("%-5d%-30s%10d%10d%10d%10d%10d", Student_Position, Student_Name, Physics_Marks, Chemistry_Marks,ICT_Marks, English_Marks, TOTAl_Marks, Average);
	}

	public int getStudent_Position() {
		return Student_Position;
	}

	public void setStudent_Position(int student_Position) {
		Student_Position = student_Position;
	}

	public String getStudent_Name() {
		return Student_Name;
	}

	public void setStudent_Name(String student_Name) {
		Student_Name = student_Name;
	}

	public int getPhysics_Marks() {
		return Physics_Marks;
	}

	public void setPhysics_Marks(int physics_Marks) {
		Physics_Marks = physics_Marks;
	}

	public int getChemistry_Marks() {
		return Chemistry_Marks;
	}

	public void setChemistry_Marks(int chemistry_Marks) {
		Chemistry_Marks = chemistry_Marks;
	}

	public int getICT_Marks() {
		return ICT_Marks;
	}

	public void setICT_Marks(int iCT_Marks) {
		ICT_Marks = iCT_Marks;
	}

	public int getEnglish_Marks() {
		return English_Marks;
	}

	public void setEnglish_Marks(int english_Marks) {
		English_Marks = english_Marks;
	}

	public int getTOTAl_Marks() {
		return TOTAl_Marks;
	}

	public void setTOTAl_Marks(int tOTAl_Marks) {
		TOTAl_Marks = tOTAl_Marks;
	}

	public double getAverage() {
		return Average;
	}

	public void setAverage(double average) {
		Average = average;
	}
	
	

	
}